import ActiveLink from "./ActiveLink";
import sidebar from "../styles/Sidebar.module.css";
import Laundry from "@material-ui/icons/LocalLaundryService";
import Dashboard from "@material-ui/icons/Dashboard";
import Avatar from "@material-ui/core/Avatar";
import Work from "@material-ui/icons/Work";
import List from "@material-ui/icons/ListAlt";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  small: {
    width: theme.spacing(4),
    height: theme.spacing(4),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));

const Sidebar = ({ toggle }) => {
  const style = {
    width: `${toggle === false ? "300px" : "80px"}`,
  };
  const none = {
    display: `${toggle === false ? "block" : "none"}`,
  };
  const classes = useStyles();
  return (
    <div style={style} className={sidebar.container}>
      <div className={sidebar.brand}>
        <Laundry fontSize="large"/>
        <h1 style={none}>Java</h1>
      </div>
      <div className={sidebar.nav}>
        <ActiveLink href="/">
          <Dashboard fontSize="small" /> <p style={none}>Dashboard</p>
        </ActiveLink>
        <ActiveLink href="/order">
          <List fontSize="small" /> <p style={none}>Order</p>
        </ActiveLink>
        <ActiveLink href="/employee">
          <Work fontSize="small" /> <p style={none}>Employee</p>
        </ActiveLink>
      </div>
    </div>
  );
};

export default Sidebar;
