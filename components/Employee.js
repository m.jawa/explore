import { useState } from "react";
import axios from "axios";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Search from "@material-ui/icons/Search";
import employee from "../styles/Employee.module.css";
import PortalClickAway from "./PortalClickAway";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Table from "./table";
import { useRouter } from "next/router";
import InputAdornment from "@material-ui/core/InputAdornment";

const Employee = ({ data }) => {
    const router = useRouter();
    const [state, setState] = useState({
        Email: "",
        Name: "",
        Phone: "",
        Address: "",
    });
    const changeInput = (e) => {
        const { value, name } = e.target;
        setState((prev) => ({
            ...prev,
            [name]: value,
        }));
    };
    const submit = () => {
        axios
            .post("http://localhost:3003/create-admin", state)
            .then(() => {
                router.reload(window.location.pathname);
            })
            .catch(() => {
                return {
                    notFound: true,
                };
            });
    };
    const [Email, setEmail] = useState(null);
    const [Name, setName] = useState(null);
    const [Address, setAddress] = useState(null);
    const [Phone, setPhone] = useState(null);
    const validate = (e) => {
        const { name, value } = e.target;
        var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (name === "Email") {
            {
                value.length > 0 && value.match(mailformat) ? setEmail(false) : setEmail(true);
            }
        } else if (name === "Name") {
            {
                value.length > 0 ? setName(false) : setName(true);
            }
        } else if (name === "Phone") {
            {
                value.length > 0 ? setPhone(false) : setPhone(true);
            }
        } else if (name === "Address") {
            {
                value.length > 0 ? setAddress(false) : setAddress(true);
            }
        }
    };
    return (
        <Container>
            <Box className={employee.box}>
                <div className={employee.Search}>
                    <Search fontSize="small" />
                    <input placeholder="Search Admin" />
                </div>
                <PortalClickAway>
                    <div className={employee.modal}>
                        <TextField
                            label="Name"
                            name="Name"
                            variant="outlined"
                            size="small"
                            onChange={changeInput}
                            onBlur={validate}
                            value={state.Name}
                            style={{ marginBottom: "20px" }}
                            error={Name}
                        />
                        <TextField
                            name="Email"
                            maxLength={12}
                            label="Email"
                            variant="outlined"
                            size="small"
                            type="email"
                            onChange={changeInput}
                            onBlur={validate}
                            value={state.Email}
                            error={Email}
                            style={{ marginBottom: "20px" }}
                        />
                        <TextField
                            name="Phone"
                            variant="outlined"
                            size="small"
                            onChange={changeInput}
                            onBlur={validate}
                            value={state.Phone}
                            style={{ marginBottom: "20px" }}
                            error={Phone}
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <span style={{ cursor: "default" }}>+62 </span>
                                    </InputAdornment>
                                ),
                            }}
                            inputProps={{
                                maxLength: 14,
                            }}
                        />
                        <TextField
                            style={{ marginBottom: "20px" }}
                            label="Address"
                            name="Address"
                            onBlur={validate}
                            value={state.Address}
                            error={Address}
                            onChange={changeInput}
                            variant="outlined"
                            size="small"
                            type="textarea"
                        />
                        <Button size="small" variant="outlined" color="primary">
                            Cancel
                        </Button>
                        <Button
                            type="submit"
                            style={{ marginTop: "5px" }}
                            size="small"
                            variant="contained"
                            color="primary"
                            onClick={submit}
                        >
                            Submit
                        </Button>
                    </div>
                </PortalClickAway>
            </Box>
            <Table data={data} />
        </Container>
    );
};

export default Employee;
