import { useRouter } from "next/router";

function ActiveLink({ children, href }) {
  const router = useRouter();
  const style = {
    marginRight: 10,
    display: "flex",
    alignItems: "center",
    borderRadius: "20px 0 0px 20px",
    padding: "15px",
    margin: "3px 0 0 15px",
    color: router.asPath === href ? "#4999f5" : "white",
    background: router.asPath === href ? "white" : "none",  
  };

  const handleClick = (e) => {
    e.preventDefault();
    router.push(href);
  };

  return (
    <a href={href} onClick={handleClick} style={style}>
      {children}
    </a>
  );
}

export default ActiveLink;
