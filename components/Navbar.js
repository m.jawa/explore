import navbar from "../styles/navbar.module.css";
import Menu from "@material-ui/icons/Menu";
import Notification from "@material-ui/icons/NotificationsNone";
import Exit from "@material-ui/icons/ExitToApp";
import IconButton from "@material-ui/core/IconButton";

const Navbar = ({ menu }) => {
  return (
    <div className={navbar.container}>
      <div className={navbar.navLeft}>
        <IconButton onClick={menu}>
          <Menu />
        </IconButton>
        <h3>Dashboard</h3>
      </div>
      <div className={navbar.navRight}>
        <IconButton>
          <Notification />
        </IconButton>
        <IconButton>
          <Exit />
        </IconButton>
      </div>
    </div>
  );
};

export default Navbar;
