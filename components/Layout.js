import { useState } from "react";
import Navbar from "./Navbar";
import Sidebar from "./Sidebar";
import useMediaQuery from "@material-ui/core/useMediaQuery";

const Layout = ({ children }) => {
  const matches = useMediaQuery("(min-width:600px)");
  const [toggle, setToggle] = useState(false);
  const Menu = () => {
    setToggle(toggle ? false : true);
  };
  return (
    <div>
      <Sidebar toggle={toggle} />
      <div style={{ marginLeft: !toggle ? "300px" : "80px" }}>
        <Navbar menu={Menu} />
        {children}
      </div>
    </div>
  );
};

export default Layout;
