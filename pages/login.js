import { useState } from "react";
import Image from "next/image";
import Login from "../styles/login.module.css";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import axios from "axios";
import cookie from "js-cookie";

const login = () => {
    const [passwordShown, setPasswordShown] = useState(false);
    const visible = () => {
        setPasswordShown(passwordShown ? false : true);
    };
    const [errMsg, setErrMsg] = useState(null);
    const [state, setState] = useState({ email: "", password: "" });
    const changeInput = (e) => {
        const { value, name } = e.target;
        setState((prev) => ({
            ...prev,
            [name]: value,
        }));
    };
    const submit = () => {
        axios
            .post("http://localhost:3003/login", state)
            .then((res) => {
                cookie.set("token", res.data.token, {
                    secure: true,
                    sameSite: "strict",
                });
                alert(res.data.token);
            })

            .catch((err) => {
                console.log(err);
            });
    };
    return (
        <div className={Login.container}>
            <div className={Login.box}>
                <div className={Login.header}>
                    <Image src="/aa.jpg" width="fit-content" height="100%" />
                </div>
                <div className={Login.body}>
                    <div>
                        <label className={Login.label}>Email</label>
                        <div className={Login.input}>
                            <input
                                name="email"
                                type="email"
                                placeholder="enter email"
                                value={state.email}
                                onChange={(e) => changeInput(e)}
                            />
                        </div>
                    </div>
                    <div>
                        <label className={Login.label}>Password</label>
                        <div className={Login.input}>
                            <input
                                name="password"
                                type="password"
                                type={passwordShown ? "text" : "password"}
                                placeholder="password"
                                value={state.password}
                                onChange={(e) => changeInput(e)}
                            />
                            {!passwordShown ? (
                                <Visibility
                                    onClick={visible}
                                    fontSize="small"
                                    style={{ cursor: "pointer", margin: "0 3px" }}
                                />
                            ) : (
                                <VisibilityOff
                                    onClick={visible}
                                    fontSize="small"
                                    style={{ cursor: "pointer", margin: "0 3px" }}
                                />
                            )}
                        </div>
                    </div>
                    <div className={Login.radioBox}>
                        <label className={Login.label}>Roles</label>
                        <div className={Login.Radio}>
                            <input id="owner" type="radio" name="Roles" value="owner" hidden />
                            <label className={Login.radio} htmlFor="owner">
                                Owner
                            </label>
                            <input id="admin" type="radio" name="Roles" value="admin" hidden />
                            <label className={Login.radio} htmlFor="admin">
                                Admin
                            </label>
                        </div>
                    </div>
                    <button onClick={submit} type="submit">
                        Login
                    </button>
                </div>
            </div>
        </div>
    );
};

export default login;
