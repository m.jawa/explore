import { useState } from "react";
import { Badge } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import set from "../styles/setPassword.module.css";
import Edit from "@material-ui/icons/Edit";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";

const setPassword = () => {
    const [passwordShow, setPasswordShow] = useState(false);
    const visible = () => {
        setPasswordShow(passwordShow ? false : true);
    };
    return (
        <div className={set.container}>
            <input id="profile" type="file" hidden />
            <div className={set.box}>
                <Badge
                    overlap="circle"
                    anchorOrigin={{
                        vertical: "bottom",
                        horizontal: "right",
                    }}
                    badgeContent={
                        <label htmlFor="profile" className={set.edit}>
                            <Edit fontSize="small" />
                        </label>
                    }
                >
                    <Avatar size="large" style={{ width: "100px", height: "100px" }} />
                </Badge>
                <p>
                    Hello, <span>Username</span>
                    <br />
                    Welcome to <i>Java's Laundry</i>
                </p>
                <div className={set.field}>
                    <div className={set.input}>
                        <input
                            type="password"
                            type={passwordShow ? "text" : "password"}
                            placeholder="Set Password"
                        />
                        {!passwordShow ? (
                            <Visibility
                                onClick={visible}
                                fontSize="small"
                                style={{ cursor: "pointer", margin: "0 3px" }}
                            />
                        ) : (
                            <VisibilityOff
                                onClick={visible}
                                fontSize="small"
                                style={{ cursor: "pointer", margin: "0 3px" }}
                            />
                        )}
                    </div>
                    <div className={set.input}>
                        <input
                            type="password"
                            type={passwordShow ? "text" : "password"}
                            placeholder="Confirm Password"
                        />
                        {!passwordShow ? (
                            <Visibility
                                onClick={visible}
                                fontSize="small"
                                style={{ cursor: "pointer", margin: "0 3px" }}
                            />
                        ) : (
                            <VisibilityOff
                                onClick={visible}
                                fontSize="small"
                                style={{ cursor: "pointer", margin: "0 3px" }}
                            />
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default setPassword;
