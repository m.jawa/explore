import Layout from "../components/Layout";
import Todo from "../components/Todo";
import style from "../styles/Todo.module.css";

const order = () => {
    return (
        <Layout>
            <div className={style.container}>
                <Todo />
                <Todo />
                <Todo />
                <Todo />
                <Todo />
            </div>
        </Layout>
    );
};

export default order;
