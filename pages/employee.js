import Layout from "../components/Layout";
import Employee from "../components/Employee";
import axios from "axios";

const employee = ({ data }) => {
    return (
        <Layout>
            <Employee data={data} />
        </Layout>
    );
};

export default employee;
export async function getServerSideProps() {
    try {
        const res = await axios.get("http://localhost:3003/get-admin");
        const data = res.data;
        console.log(data);
    } catch (error) {
        console.log(error);
        return {
            notFound: true,
        };
    }
}
